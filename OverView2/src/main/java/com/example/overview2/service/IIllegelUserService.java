package com.example.overview2.service;

import com.example.overview2.entity.IllegelUser;

import java.util.List;

/**
 * Created by Administrator on 2018/6/10.
 *
 * @Author:
 * @Description:
 */
public interface IIllegelUserService {
    public List<IllegelUser> getAllIllegelUsers();

}

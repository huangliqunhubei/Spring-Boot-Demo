package com.example.overview2.service.Impl;

import com.example.overview2.entity.IllegelUser;
import com.example.overview2.mapper.IllegelUserMapper;
import com.example.overview2.service.IIllegelUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2018/6/10.
 *
 * @Author:
 * @Description:
 */
@Service
public class IllegelUserServiceImpl implements IIllegelUserService {

    @Autowired
    private IllegelUserMapper illegelUserMapper;

    @Override
    public List<IllegelUser> getAllIllegelUsers() {
        return illegelUserMapper.selectAllUser();
    }
}

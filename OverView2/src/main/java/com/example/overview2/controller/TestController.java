package com.example.overview2.controller;

import com.example.overview2.entity.IllegelUser;
import com.example.overview2.service.IIllegelUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by Administrator on 2018/6/10.
 *
 * @Author:
 * @Description:
 */
@Controller
@RequestMapping("/test")
public class TestController {
 @Autowired
 private IIllegelUserService iIllegelUserService;

    @RequestMapping("/allusers")
    public String  getAllIllegelUser(){
        List<IllegelUser> list = iIllegelUserService.getAllIllegelUsers();
        for(IllegelUser user:list){
            System.out.println(user.getUserName());
        }
        return "index";
    }

    @RequestMapping("/login")
    public String Login(){
        return "pages/login";
    }
}

package com.example.overview2.mapper;

import com.example.overview2.entity.IllegelUser;

import java.util.List;

public interface IllegelUserMapper {
    int deleteByPrimaryKey(String userId);

    int insert(IllegelUser record);

    int insertSelective(IllegelUser record);

    IllegelUser selectByPrimaryKey(String userId);

    int updateByPrimaryKeySelective(IllegelUser record);

    int updateByPrimaryKey(IllegelUser record);

    List<IllegelUser> selectAllUser();
}
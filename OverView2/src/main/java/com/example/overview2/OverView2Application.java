package com.example.overview2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@MapperScan("com.example.overview2.mapper")
@ComponentScan({"com.example.overview2.controller","com.example.overview2.service"})
public class OverView2Application {

	public static void main(String[] args) {
		SpringApplication.run(OverView2Application.class, args);
	}
}
